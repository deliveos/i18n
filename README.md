# Instalation
```bash
go get gitlab.com/deliveos/i18n
```

# Informaton
`Localize` method accepts `key` specified in the file and `locale` from `NewLocalizer` or `AddLocale`. __Not name of a file!__

# Errors
## "Value for the key "\<key\>" is not specified in the \<path\>"
Method `AddLocale` checks the added file and attempts to find all keys specified in the file of dafault locale. To avoid this error need to add the key and a value to the specified file.
## Undefined key "\<key\>"
Method `Localize` attempts to find value by the key from file of specified locale and, if it is not found attempts to find it in the file of default locale. It means the key was not found in the both cases.

# Examples
`main.go`
```go
package main

import (
	"fmt"

	"gitlab.com/deliveos/i18n"
	"gopkg.in/yaml.v3"
)

func main() {
  // Initialize localizer
	l := i18n.NewLocalizer("en", "./i18n/en.yaml", yaml.Unmarshal)
  // Add localization
	l.AddLocale("ru", "./i18n/ru.yaml", yaml.Unmarshal)
  // Get localized text
	fmt.Print(l.Localize("helloWorld", "en")) // Hello World!
}

```
`./i18n/en.yaml`
```yaml
helloWorld: Hello World!
```
`./i18n/ru.yaml`
```yaml
helloWorld: Привет мир!
```