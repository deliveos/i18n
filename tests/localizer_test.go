package tests

import (
	"testing"

	"gitlab.com/deliveos/i18n"
	"gopkg.in/yaml.v3"
)

func TestLocalizer(t *testing.T) {
	l := i18n.NewLocalizer("en", "./i18n/en.yaml", yaml.Unmarshal)
	l.AddLocale("ru", "./i18n/ru.yaml", yaml.Unmarshal)
	t.Log(l.Localize("helloWorld", "en"))
}
