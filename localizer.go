package i18n

import (
	"fmt"
	"os"
)

// UnmarshalFunc decodes within the byte cut and assigns decoded values in the out value v.
type UnmarshalFunc func(data []byte, v interface{}) error

// Localizer provides Localize methods that return localized strings.
type Localizer struct {
	defaultLanguage string
	templates       map[string]map[string]string
}

// Returns new localizer with default language
func NewLocalizer(defaultLanguage string, path string, unmarshalFunc UnmarshalFunc) *Localizer {
	var l = &Localizer{
		defaultLanguage: defaultLanguage,
		templates:       make(map[string]map[string]string),
	}

	l.setLocale(defaultLanguage, path, unmarshalFunc)
	return l
}

// Adds locale from file
func (l *Localizer) AddLocale(locale string, path string, unmarshalFunc UnmarshalFunc) *Localizer {
	l.setLocale(locale, path, unmarshalFunc)
	for key := range l.templates[l.defaultLanguage] {
		if l.templates[locale][key] == "" {
			panic(fmt.Sprintf("Value for the key \"%s\" is not specified in the %s", key, path))
		}
	}
	return l
}

// Returns localized string
func (l *Localizer) Localize(key string, locale string) string {
	text := l.templates[locale][key]
	if text == "" {
		text = l.templates[l.defaultLanguage][key]
		if text == "" {
			panic(fmt.Sprintf("Undefined key \"%s\"", key))
		}
	}
	return text
}

func (l *Localizer) setLocale(locale string, path string, unmarshal UnmarshalFunc) {
	content, err := os.ReadFile(path)
	if err != nil {
		panic(err)
	}
	var template = map[string]string{}
	unmarshal(content, template)
	l.templates[locale] = template
}
